/// <reference types="cypress" />

import TelaHome from '../../support/pageobjects/TelaHomePO.js'
const telaHome = new TelaHome()

/*
@autor:Paulo Henrique
@Data:01/08/2022
@Descrição da funcionalidade: Essa funcionalidade é responsavel por realizar os testes responsavel por uma determinada funcionalidade
*/
describe('Funcionalidade: Dasa', {tags:['@full']},()=>{
    /*
    Before: responsavel por executar antes dos cenário
    */
    before('Contexto: Acessar a mesma pre-condição', async () => {
        telaHome.acessarSite(url)
    });

    /*
    @Autor:Paulo Henrique
    @Data:
    @Escreva seu BDD aqui não esqueça do \n para saltar a linha de cada step
    @Teste responsavel por acessar telas do menu inicial
    */
    it(`Dado que tenha Pré Condição\n 
        Quando tiver uma ação\n
        Então tenho o resultado final`, {tags:['@Login', '@DS0001']},()=>{
            /*Aqui iremos chamar nossas Pages Action (ver nomeclatura)
                Ex:
            */
    });

    it(`Dado que tenha Pré Condição\n 
        Quando tiver uma ação\n
        Então tenho o resultado final`, {tags:['@Login', '@DS0002']},()=>{
            //Aqui iremos chamar nossas Pages Action (ver nomeclatura)
    });
       
});