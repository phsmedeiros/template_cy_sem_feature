/*
Classe Locators/PageObject 
Desc.: Classe responsavel por referenciar o elementos da tela
Elementos da pagina Home
*/
class TelaHomeLcts {
    
    paginaHome = () => { return '.sc-15silij-0 > .sc-1xia3sz-0 > :nth-child(2)'}
    botaoSomosDasa = () => { return 'a[href=/somos-dasa"]'}
    botaoParaMedico = () => { return 'a[href="/medicos"]'}
    botaoParaPacientes = () => { return 'a[href="/pacientes"]'}
    botaoParaEmpresas = () => { return 'a[href="/empresas"]'}
    botaoParaInvestidor = () => { return 'a[href="https://dasa3.com.br/"]'}
    validarTelaHome = () => { return '#___gatsby h1'}
}

export default TelaHomeLcts;