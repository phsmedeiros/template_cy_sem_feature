const url = Cypress.config("baseUrl")
import LocatorsHome from '../locators/TelaHomeLcts.js'
const locatorHome = new LocatorsHome()
import Utils from '../common/Utils.js'
const utils = new Utils()

/*
Classe pageObject/pageAction
Desc.: Classe responsavel pelos metodos de ações da pagina.

*/
class TelaHome {

    // Acessa o site que será testado
    acessarSite(url) {
        cy.visit(url)
        cy.reload()
        utils.aguardarPagina(locatorHome.validarTelaHome, 'Somos muitosSomos Dasa')
    }

    //Clica nos botões principais da tela home
    acessarTelas(nomeTela){
        switch (nomeTela.trim().toUpperCase()) {
            case 'DASA':
                utils.eventoClick(locatorHome.botaoSomosDasa)
              break;
            case 'MEDICOS':
            case 'MÉDICOS':
                utils.eventoClick(locatorHome.botaoParaMedico)
                break;
            case 'PACIENTES':
            case 'PACIÊNTES':
                utils.eventoClick(locatorHome.botaoParaPacientes)
              break;
            case 'EMPRESAS':
                utils.eventoClick(locatorHome.botaoParaEmpresas)         
              break;
            case 'INVESTIDOR':
                utils.eventoClick(locatorHome.botaoParaInvestidor)          
              break;
            default:
              assert.notExists(null,'Nome da Tela Informada: ' & nomeTela & ' não existe')
          }
    }
}
export default TelaHome;