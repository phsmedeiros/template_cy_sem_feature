let count = 0
/*
Classe Utils 
Desc.: Classe responsavel por metodos genericos que possa atender todas as necessidades do projeto.
*/
class Utils {
    
    //Metodo responsavel por um evento de click em botão.
    eventoClick(btn){
       this.aguardarElemento(btn)
       cy.get(btn).pause().click()
    }
   
    //Metodo responsavel por aguardar o elemento existir na tela.
    aguardarElemento(objeto){
        while (!cy.get(objeto).should('exist') && count < 10) {
            cy.wait(1000)
            count = count + 1
        }
    }

    //Metodo responsavel pro aguardar paginas carregar.
    aguardarPagina(elementoPagina, valor){
        while (!cy.get(elementoPagina).should('have.text', valor) && count < 10) {
            cy.wait(1000)
            count = count + 1
        }
    }

}

export default Utils;